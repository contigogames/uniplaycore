﻿using System;
using System.Runtime.CompilerServices;
using UniPlay.Core.Actor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UniPlay.Core.GameObject
{
    public static class ActorHelperExtensions
    {
        private static readonly ConditionalWeakTable<Object, WeakReference<ActorBinder>> s_quickBinderLookup =
            new ConditionalWeakTable<Object, WeakReference<ActorBinder>>();

        /// <summary>
        /// Get the <see cref="IActor"/> for a given unity object.
        /// </summary>
        /// <remarks>This method will save the lookup result in a weak reference cache.</remarks>
        public static IActor GetActor(this Object self)
        {
            return GetActorBinding(self)?.Actor;
        }

        /// <summary>
        /// Gets the <see cref="IActorBinding"/> of a unity object.
        /// </summary>
        public static IActorBinding GetActorBinding(this Object self)
        {
            return self.GetBinder()?.CurrentBinding;
        }

        public static ActorBinder GetBinder(this Object self)
        {
            var actorBinder = self as ActorBinder;
            if (actorBinder != null)
            {
                return actorBinder;
            }
            
            var reference =  s_quickBinderLookup.GetValue(
                self,
                key =>
                {
                    var component = self as Component;
                    if (component != null)
                    {
                        return new WeakReference<ActorBinder>(component.gameObject.GetBinder());
                    }

                    var gameObject = self as UnityEngine.GameObject;
                    if (gameObject != null)
                    {
                        var binding = gameObject.GetComponent<ActorBinder>();
                        if (binding != null)
                        {
                            return new WeakReference<ActorBinder>(binding);
                        }

                        if (gameObject.transform.parent != null)
                        {
                            return new WeakReference<ActorBinder>(gameObject.transform.parent.GetBinder());
                        }
                    }

                    return null;
                });

            if (reference == null)
            {
                return null;
            }

            ActorBinder outBinder;
            return reference.TryGetTarget(out outBinder) ? outBinder : null;
        }
    }
}