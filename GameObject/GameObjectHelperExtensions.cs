﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UniPlay.Core.GameObject
{
    /// <summary>
    /// Helper extensions for <see cref="GameObject"/>s.
    /// </summary>
    public static class GameObjectHelperExtensions
    {
        /// <summary>
        /// Get the <see cref="GameObject"/> assosiated with a <see cref="Object"/>.
        /// </summary>
        public static UnityEngine.GameObject GetGameObject(this Object obj)
        {
            var go = obj as UnityEngine.GameObject;
            if (go != null)
            {
                return go;
            }

            var component = obj as Component;
            if (component != null)
            {
                return component.gameObject;
            }

            return null;
        }

        public static IEnumerable<UnityEngine.GameObject> GetAllFromRoot(
            this UnityEngine.GameObject root) {
            for (int i = 0; i < root.transform.childCount; i++) {
                foreach (var child in GetAllFromRoot(root.transform.GetChild(i).gameObject)) {
                    yield return child;
                }
            }

            yield return root;
        }

        public static IEnumerable<UnityEngine.GameObject> GetAllFromRoots(
            this IEnumerable<UnityEngine.GameObject> roots) {
            return roots.SelectMany(GetAllFromRoot);
        }
    }
}