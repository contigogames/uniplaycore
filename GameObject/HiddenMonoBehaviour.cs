﻿using UnityEngine;

namespace UniPlay.Core.GameObject
{
    /// <summary>
    /// A <see cref="MonoBehaviour"/> that will automatically hide itself in the inspector.
    /// </summary>
    public abstract class HiddenMonoBehaviour : MonoBehaviour
    {
        private void OnValidate()
        {
            hideFlags = HideFlags.HideInInspector;
        }

        private void Awake()
        {
            hideFlags = HideFlags.HideInInspector;
        }
    }
}