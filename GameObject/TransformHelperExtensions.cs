using System.Collections.Generic;
using UnityEngine;

namespace UniPlay.Core.GameObject {
    public static class TransformHelperExtensions {
        public static IEnumerable<Transform> LeafTransforms(this Transform transform) {
            if (transform.childCount == 0) {
                yield return transform;
            }

            for (int i = 0; i < transform.childCount; i++) {
                var child = transform.GetChild(i);
                foreach (var leafTransform in child.LeafTransforms()) {
                    yield return leafTransform;
                }
            }
        }
    }
}