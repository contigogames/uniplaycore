namespace UniPlay.Core.Tasks {
    /// <summary>
    /// Represents a promise that needs to be fulfilled in stages.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct PartialPromise<TThisPromise, TNextPromise> {
        public Promise<TNextPromise> NextPromise;

        public TThisPromise ThisResult;
    }
}
