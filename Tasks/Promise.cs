﻿using System;

namespace UniPlay.Core.Tasks
{
    /// <summary>
    /// A promise of a value being returned in the future.
    /// </summary>
    /// <typeparam name="T">The type of value being returned.</typeparam>
    public class Promise<T>
    {
        private T m_fullfillment;

        private Action<T> onFullfilled;
        private Action onBroken;

        /// <summary>
        /// Called when the return value has been fullfilled.
        /// </summary>
        public event Action<T> OnFullfilled
        {
            add
            {
                if (HasBeenFullfilled)
                {
                    value?.Invoke(m_fullfillment);
                    return;
                }
                
                onFullfilled += value;
            }
            remove { onFullfilled -= value; }
        }

        /// <summary>
        /// Called if the promise is borken.
        /// </summary>
        public event Action OnBroken
        {
            add
            {
                if (HasBeenBroken)
                {
                    value?.Invoke();
                    return;
                }

                onBroken += value;
            }
            remove { onBroken -= value; }
        }

        /// <summary>
        /// The value of the fullfillment.
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown if the fullfillment has not been made.</exception>
        public T Fullfillment
        {
            get
            {
                if (HasBeenBroken)
                {
                    throw new InvalidOperationException("The promise has been broken and has no fullfillment.");
                }
                
                if (!HasBeenFullfilled)
                {
                    throw new InvalidOperationException("Cannot access fullfillment before it has been finished.");
                }

                return m_fullfillment;
            }
        }

        /// <summary>
        /// If the return has been fullfilled yet.
        /// </summary>
        public bool HasBeenFullfilled { get; private set; }

        public bool HasBeenBroken { get; private set; }

        public bool HasFinished
        {
            get { return HasBeenBroken || HasBeenFullfilled; }
        }

        /// <summary>
        /// Fullfilles the promise.
        /// </summary>
        /// <param name="value">The value to fullfill with.</param>
        /// <exception cref="InvalidOperationException">Thrown if the promise has already been fullfilled.</exception>
        public void Fullfill(T value)
        {
            m_fullfillment = value;
            HasBeenFullfilled = true;

            onFullfilled?.Invoke(value);
        }

        public void Break()
        {
            HasBeenBroken = true;
            
            onBroken?.Invoke();
        }
    }
}