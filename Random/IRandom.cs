﻿using System.Collections.Generic;

namespace UniPlay.Core.Random
{
    /// <summary>
    /// Generates random numbers on a certain distribution.
    /// </summary>
    public interface IRandomDistribution<T>
    {
        /// <summary>
        /// Generates an infinate enumeration of random numbers.
        /// </summary>
        /// <returns>An infinate enumeration of random numbers.</returns>
        IEnumerable<T> Generate();

        /// <summary>
        /// Get the next random number.
        /// </summary>
        /// <returns>The next random number.</returns>
        T GetNext();
    }
}