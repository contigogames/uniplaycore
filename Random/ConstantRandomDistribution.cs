﻿using System.Collections.Generic;
using System.Linq;
using UniPlay.Core.Verification;

namespace UniPlay.Core.Random
{
    /// <summary>
    /// A <see cref="IRandomDistribution"/> that will loop over a constant set of numbers.
    /// </summary>
    public class ConstantRandomDistribution : IRandomDistribution<float>
    {
        private readonly float[] m_randomNumbers;

        private int m_index;

        /// <param name="randomNumbers">A collection of numbers to loop over.</param>
        public ConstantRandomDistribution(ICollection<float> randomNumbers)
        {
            randomNumbers.CheckIsNotEmpty();
            m_randomNumbers = randomNumbers.ToArray();
        }

        /// <inheritdoc />
        public IEnumerable<float> Generate()
        {
            while (true)
            {
                yield return GetNext();
            }
        }

        /// <inheritdoc />
        public float GetNext()
        {
            return m_randomNumbers[GetNextIndex()];
        }

        private int GetNextIndex()
        {
            m_index = (m_index + 1) % m_randomNumbers.Length;
            return m_index;
        }
    }
}