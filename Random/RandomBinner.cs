using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace UniPlay.Core.Random {
    public sealed class RandomBinner : IRandomDistribution<int> {
        private readonly IRandomDistribution<float> _generator;
        private readonly int _min;
        private readonly int _max;

        public RandomBinner(IRandomDistribution<float> generator, int min, int max) {
            Assert.IsNotNull(generator);
            Assert.IsTrue(max > min, "max > min");
            
            _generator = generator;
            _min = min;
            _max = max;
        }
        
        public IEnumerable<int> Generate() {
            while (true) {
                yield return GetNext();
            }
        }

        public int GetNext() {
            var range = _max - _min;
            var rOnRange = _generator.GetNext() * range;
            return Mathf.Clamp((int) rOnRange + _min, _min, _max - 1);
        }
    }
}
