﻿using System.Collections.Generic;
using System.Linq;

namespace UniPlay.Core.Random
{
    /// <summary>
    /// A <see cref="IRandomDistribution"/> wrapper than can scale and shift another random distribution.
    /// </summary>
    public class TransformedRandomDistribution : IRandomDistribution<float>
    {
        private readonly float m_scale;
        private readonly float m_shifted;
        private readonly IRandomDistribution<float> m_inner;

        /// <param name="inner">The inner distribution to transform.</param>
        /// <param name="scale">How much to scale the distribution.</param>
        /// <param name="shifted">How much to shift the distribution.</param>
        public TransformedRandomDistribution(IRandomDistribution<float> inner, float scale = 1f, float shifted = 0f)
        {
            m_inner = inner;
            m_scale = scale;
            m_shifted = shifted;
        }

        /// <inheritdoc />
        public IEnumerable<float> Generate()
        {
            return m_inner.Generate().Select(f => (f * m_scale) + m_shifted);
        }

        /// <inheritdoc />
        public float GetNext()
        {
            return (m_inner.GetNext() * m_scale) + m_shifted;
        }
    }
}