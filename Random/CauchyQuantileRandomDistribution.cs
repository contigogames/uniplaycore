using System;
using System.Collections.Generic;
using UnityEngine;

namespace UniPlay.Core.Random {
    public class CauchyQuantileRandomDistribution : IRandomDistribution<float> {
        private readonly float _bounds;
        private readonly System.Random _random = new System.Random();

        public CauchyQuantileRandomDistribution(float bounds = 6.5f) {
            _bounds = bounds;
        }

        public IEnumerable<float> Generate() {
            while (true) {
                yield return GetNext();
            }
        }

        public float GetNext() {
            var nextUniform = (float) _random.NextDouble();
            var unAdjustedValue = Mathf.Tan(Mathf.PI * (nextUniform - 0.5f));
            if (unAdjustedValue > _bounds || unAdjustedValue < -_bounds) {
                return GetNext();
            }

            var value = (unAdjustedValue / _bounds) * 0.5f + 0.5f;
            return value;
        }
    }
}
