﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

namespace UniPlay.Core.Random
{
    /// <summary>
    /// Generates random numbers on a uniform distribution.
    /// </summary>
    /// <remarks>
    /// https://en.wikipedia.org/wiki/Uniform_distribution_(continuous)
    /// </remarks>
    public class UniformRandomDistribution : IRandomDistribution<float>
    {
        private static UniformRandomDistribution s_isntance =
            new UniformRandomDistribution();

        /// <summary>
        /// Generates a random number within a range.
        /// </summary>
        /// <param name="min">The min value for the number.</param>
        /// <param name="max">The max value for the number.</param>
        /// <returns>A random number between min and max.</returns>
        public static float GetRandomInRange(float min, float max)
        {
            s_isntance.m_min = min;
            s_isntance.m_max = max;

            return s_isntance.GetNext();
        }

        /// <summary>
        /// Generates a random integer within the range [min, max)
        /// </summary>
        /// <param name="min">The min bound for the number.</param>
        /// <param name="max">The max bound for the number (exclusive).</param>
        /// <returns></returns>
        public static int GetRandomInRange(int min, int max)
        {
            return (int) GetRandomInRange((float) min, (float) max);
        }

        /// <summary>
        /// Gets a random true/false value.
        /// </summary>
        /// <returns>A random true false value.</returns>
        public static bool GetRandomBool()
        {
            return GetRandomInRange(0, 2) == 0;
        }

        public static T GetRandomFrom<T>(IList<T> list)
        {
            Assert.AreNotEqual(list.Count, 0);
            var index = GetRandomInRange(0, list.Count);
            return list[index];
        }

        public static IEnumerable<T> GetContinousRandomFrom<T>(IList<T> list)
        {
            while (true)
            {
                yield return GetRandomFrom(list);
            }
        }

        private System.Random m_generator;
        private float m_min, m_max;

        /// <param name="min">The lower bound of the range.</param>
        /// <param name="max">The upper bound of the range (exclusive).</param>
        public UniformRandomDistribution(
            float min = 0f,
            float max = 1f)
            : this(new System.Random(), min, max)
        {
        }

        /// <param name="generator">The random number generator to use.</param>
        /// <param name="min">The lower bound of the range.</param>
        /// <param name="max">The upper bound of the range (exclusive).</param>
        public UniformRandomDistribution(
            System.Random generator,
            float min = 0f,
            float max = 1f)
        {
            m_generator = generator;
            m_min = min;
            m_max = max;
        }

        /// <inheritdoc />
        public IEnumerable<float> Generate()
        {
            while (true)
            {
                yield return GetNext();
            }
        }

        /// <inheritdoc />
        public float GetNext()
        {
            var range = m_max - m_min;
            var x = (float) m_generator.NextDouble();

            return (x * range) + m_min;
        }
    }
}