﻿using UniPlay.Core.Actor;
using UniPlay.Core.View;

namespace UniPlay.Core.Signal
{
    /// <summary>
    /// A signal that can be raised on a <see cref="IActor"/>.
    /// </summary>
    public interface ISignal : IView { }
}