﻿using UniPlay.Core.Actor;
using UniPlay.Core.View;

namespace UniPlay.Core.Signal
{
    /// <summary>
    /// A class that can send a signal to a <see cref="IActor"/>
    /// </summary>
    public interface ISignalSender : IView { }
}