using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UniPlay.Core.Linq {
    public static class MoreEnumerable {
        public static IEnumerable<T> Loop<T>(this IEnumerable<T> toLoop, Func<bool> conditional) {
            while (conditional == null || conditional()) {
                foreach (var v in toLoop) {
                    yield return v;
                }
            }
        }
        
        public static IEnumerable<T> Singleton<T>(Func<T> selector) {
            yield return selector();
        }

        public static IEnumerable<T> Append<T>(this IEnumerable<T> e, Func<T> selector) {
            foreach (var v in e) {
                yield return v;
            }

            yield return selector();
        }

        public struct Condition<T> {
            public Func<bool> If { get; set; }
            public IEnumerable<T> Then { get; set; }
        }

        public static IEnumerable<T> Conditional<T>(this IEnumerable<T> extending, params Condition<T>[] conditions) {
            foreach (var v in extending) {
                yield return v;
            }

            IEnumerable<T> next = null;

            foreach (var condition in conditions) {
                if (condition.If == null) {
                    next = condition.Then;
                } else if (condition.If()) {
                    next = condition.Then;
                }
            }

            if (next != null) {
                foreach (var v in next) {
                    yield return v;
                }
            }
        }

        public static IEnumerable<T> Conditional<T>(params Condition<T>[] conditions) {
            return Enumerable.Empty<T>().Conditional(conditions);
        }

        public static IEnumerable<T> Lookup<T, TKey>(this IEnumerable<T> extending,
            Func<TKey> keySelector,
            Dictionary<TKey, IEnumerable<T>> lookup) {
            foreach (var v in extending) {
                yield return v;
            }

            IEnumerable<T> next;
            if (!lookup.TryGetValue(keySelector(), out next)) {
                next = Enumerable.Empty<T>();
            }

            foreach (var v in next) {
                yield return v;
            }
        }

        public static IEnumerable<T> Lookup<T, TKey>(Func<TKey> keySelector,
            Dictionary<TKey, IEnumerable<T>> lookup) {
            return Enumerable.Empty<T>().Lookup(keySelector, lookup);
        }
    }
}