﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalCoroutineRunner : MonoBehaviour
{

    public static GameObject runnerGameObject = null;
    public static GlobalCoroutineRunner runner = null;

    public static Coroutine RunCoroutine(IEnumerator coroutine)
    {
        if (runnerGameObject == null)
        {
            runnerGameObject = new GameObject("runner");
            DontDestroyOnLoad(runnerGameObject);
            runner = runnerGameObject.AddComponent<GlobalCoroutineRunner>();
        }

        return runner.StartCoroutine(runner.MonitorRunning(coroutine));
    }

    IEnumerator MonitorRunning(IEnumerator coroutine)
    {
        while (coroutine.MoveNext())
        {
            yield return coroutine.Current;
        }
    }

    public static void StopStaticCoroutine(Coroutine enumerator)
    {
        runner.StopCoroutine(enumerator);
    }
}
