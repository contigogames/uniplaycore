﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace UniPlay.Core.Engine.Startup
{
    /// <summary>
    /// Handles loading startup logic.
    /// </summary>
    internal static class StartupBootloader
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Startup()
        {
            var methods =
            (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                from type in assembly.GetTypes()
                from methodInfo in type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)
                where methodInfo.GetCustomAttribute<StartupSystem>() != null
                select methodInfo).OrderBy(info => info.GetCustomAttribute<StartupSystem>().Order);

            foreach (var methodInfo in methods)
            {
                try
                {
                    methodInfo.Invoke(null, Array.Empty<object>());
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }
}