﻿using System;

namespace UniPlay.Core.Engine.Startup
{
    /// <summary>
    /// Any static method marked with this attribute will be run at startup.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class StartupSystem : Attribute
    {
        public int Order { get; }

        /// <param name="order">A relative order to be run in.</param>
        public StartupSystem(int order = 0)
        {
            Order = order;
        }
    }
}