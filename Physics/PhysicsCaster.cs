﻿using UnityEngine;

namespace UniPlay.Core.Physics
{
    /// <summary>
    /// <see cref="ICaster"/> that will cast using 3d physics.
    /// </summary>
    public sealed class PhysicsCaster : ICaster
    {
        private readonly LayerMask layerMask;
        private readonly QueryTriggerInteraction triggerInteraction;

        /// <summary>
        /// Creates a <see cref="PhysicsCaster"/>.
        /// </summary>
        /// <param name="layerMask">The layer(s) to cast on.</param>
        /// <param name="triggerInteraction">How the caster will interact with triggers.</param>
        public PhysicsCaster(
            LayerMask layerMask,
            QueryTriggerInteraction triggerInteraction = QueryTriggerInteraction.UseGlobal)
        {
            this.layerMask = layerMask;
            this.triggerInteraction = triggerInteraction;
        }

        public CastResult? Raycast(Ray ray, float distance)
        {
            RaycastHit hit;
            if (UnityEngine.Physics.Raycast(ray, out hit, distance, layerMask, triggerInteraction))
            {
                return new CastResult()
                {
                    ExtRaycastHit = hit,
                    ExtRaycastHit2D = null,
                    HitPoint = hit.point,
                    HitTransform = hit.transform,
                    Normal = hit.normal
                };
            }

            return null;
        }

        public CastResult? Linecast(Vector3 start, Vector3 end)
        {
            RaycastHit hit;
            if (UnityEngine.Physics.Linecast(start, end, out hit, layerMask, triggerInteraction))
            {
                return new CastResult()
                {
                    ExtRaycastHit = hit,
                    ExtRaycastHit2D = null,
                    HitPoint = hit.point,
                    HitTransform = hit.transform,
                    Normal = hit.normal
                };
            }

            return null;
        }

        public CastResult? Spherecast(Ray ray, float radius, float distance)
        {
            RaycastHit hit;
            if (UnityEngine.Physics.SphereCast(ray, radius, out hit, distance, layerMask, triggerInteraction))
            {
                return new CastResult()
                {
                    ExtRaycastHit = hit,
                    ExtRaycastHit2D = null,
                    HitPoint = hit.point,
                    HitTransform = hit.transform,
                    Normal = hit.normal
                };
            }

            return null;
        }
    }
}