﻿using UnityEngine;

namespace UniPlay.Core.Physics
{
    /// <summary>
    /// The results of a physics cast.
    /// </summary>
    public struct CastResult
    {
        /// <summary>
        /// Where a cast hit.
        /// </summary>
        public Vector3 HitPoint;
        
        /// <summary>
        /// The normal of a cast.
        /// </summary>
        public Vector3 Normal;

        /// <summary>
        /// The transform that was hit.
        /// </summary>
        public Transform HitTransform;

        /// <summary>
        /// If a 3d cast, this info will be given.
        /// </summary>
        public RaycastHit? ExtRaycastHit;
        
        /// <summary>
        /// If a 2d cast, this info will be given.
        /// </summary>
        public RaycastHit2D? ExtRaycastHit2D;
    }
    
    public interface ICaster
    {
        /// <summary>
        /// A raycast into the world.
        /// </summary>
        /// <param name="ray">The ray to cast.</param>
        /// <param name="distance">The distance to cast the ray.</param>
        /// <returns>A raycast result, or null if nothing is hit.</returns>
        CastResult? Raycast(Ray ray, float distance);

        /// <summary>
        /// A linecast into the world.
        /// </summary>
        /// <param name="start">The start of the cast.</param>
        /// <param name="end">The end of the cast.</param>
        /// <returns>The raycast result, or null if nothing is hit.</returns>
        CastResult? Linecast(Vector3 start, Vector3 end);

        /// <summary>
        /// A spherecast into the world.
        /// </summary>
        /// <param name="ray">The ray to cast the sphere on.</param>
        /// <param name="radius">The radius of the sphere.</param>
        /// <param name="distance">The max distance to cast the sphere.</param>
        /// <returns>The raycast result, or null if nothing was hit.</returns>
        CastResult? Spherecast(Ray ray, float radius, float distance);
    }
}