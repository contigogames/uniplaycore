﻿namespace UniPlay.Core.Constants
{
    /// <summary>
    /// Constants for floaitng points.
    /// </summary>
    public static class FloatConstants
    {
        /// <summary>
        /// Default tolerance for comparing floats.
        /// </summary>
        public const float DefaultTolerance = 0.00001f;
    }
}