﻿using UnityEngine;

namespace UniPlay.Core.Constants
{
    /// <summary>
    /// Constants for <see cref="Vector3"/>
    /// </summary>
    public static class Vector3Constants
    {
        /// <summary>
        /// Default tolerance for comparing magnitudes.
        /// </summary>
        public const float DefaultMagnitudeTolerance = 0.00001f;
    }
}