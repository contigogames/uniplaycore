﻿using UnityEngine;

namespace UniPlay.Core.Constants
{
    /// <summary>
    /// Constants for <see cref="Vector2"/>
    /// </summary>
    public static class Vector2Constants
    {
        /// <summary>
        /// Default tolerance for comparing magnitudes.
        /// </summary>
        public const float DefaultMagnitudeTolerance = 0.00001f;
    }
}