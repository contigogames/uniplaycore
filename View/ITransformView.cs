﻿using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A <see cref="IView"/> of an objects <see cref="Transform"/>.
    /// </summary>
    public interface ITransformView : IView
    {
        Vector3 Position { get; set; }
        
        Quaternion Rotation { get; set; }
        
        Vector3 Up { get; set; }
        
        Vector3 Right { get; set; }
        
        Vector3 Forward { get; set; }
    }
}