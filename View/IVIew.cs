﻿using UniPlay.Core.Actor;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A view of the physical object that the <see cref="IActor"/> controls.
    /// </summary>
    public interface IView { }
}