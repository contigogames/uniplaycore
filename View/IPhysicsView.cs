﻿using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// <see cref="IView"/> of an objects physics.
    /// </summary>
    public interface IPhysicsView
    {
        Vector3 Position { get; set; }
        Quaternion Rotation { get; set; }

        Vector3 Velocity { get; set; }
        Vector3 AngularVelocity { get; set; }

        float Mass { get; }
    }
}