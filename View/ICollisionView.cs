﻿using System;
using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A view of an objects collision.
    /// </summary>
    public interface ICollisionView : IView
    {
        /// <summary>
        /// Raised when an object collides.
        /// </summary>
        event Action<Collider> OnCollisionEntered;
        
        /// <summary>
        /// Raised when an object stays collided.
        /// </summary>
        event Action<Collider> OnCollisionStayed;
        
        /// <summary>
        /// Raised when an object stops colliding.
        /// </summary>
        event Action<Collider> OnCollisionExited;
    }
}