﻿using System;
using UniPlay.Core.GameObject;
using UnityEngine;

namespace UniPlay.Core.View
{
    [DisallowMultipleComponent]
    public sealed class GameObjectView
        : MonoBehaviour,
            IGameObjectView
    {
        [SerializeField]
        private UnityEngine.GameObject m_gameObjectOverride;

        public event Action<GameObjectView> OnStart;
        public event Action<GameObjectView> OnEnabled;
        public event Action<GameObjectView> OnDisabled;
        public event Action<GameObjectView> OnDestroyed;

        public UnityEngine.GameObject Root
        {
            get
            {
                if (m_gameObjectOverride != null)
                {
                    return m_gameObjectOverride;
                }

                return gameObject;
            }
        }

        private void Start()
        {
            OnStart?.Invoke(this);
        }

        private void OnEnable()
        {
            OnEnabled?.Invoke(this);
        }

        private void OnDisable()
        {
            OnDisabled?.Invoke(this);
        }

        private void OnDestroy()
        {
            OnDestroyed?.Invoke(this);
        }

        public void Destroy()
        {
            Destroy(Root);
        }
    }
}