﻿using UniPlay.Core.GameObject;
using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A <see cref="IPhysicsView"/> of a 2d physics object.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public sealed class Physics2DView
        : HiddenMonoBehaviour,
            IPhysicsView
    {
        private Rigidbody2D m_rigidbody;

        private void Start()
        {
            m_rigidbody = GetComponent<Rigidbody2D>();
        }

        public Vector3 Position
        {
            get { return m_rigidbody.position; }
            set { m_rigidbody.position = value; }
        }

        public Quaternion Rotation
        {
            get { return Quaternion.Euler(0f, 0f, m_rigidbody.rotation); }
            set { m_rigidbody.rotation = value.eulerAngles.z; }
        }

        public Vector3 Velocity
        {
            get { return m_rigidbody.velocity; }
            set { m_rigidbody.velocity = value; }
        }

        public Vector3 AngularVelocity
        {
            get { return new Vector3(0f, 0f, m_rigidbody.angularVelocity); }
            set { m_rigidbody.angularVelocity = value.z; }
        }

        public float Mass
        {
            get { return m_rigidbody.mass; }
        }
    }
}