﻿using UniPlay.Core.Ticking;
using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A <see cref="ITickView"/> that can be created dynamically and tied to any <see cref="IGameObjectView"/>.
    /// </summary>
    public class GameObjectTickView : ITickView
    {
        public ITicker Ticker { get; }

        public GameObjectTickView(IGameObjectView gameObjectView, ITicker ticker)
        {
            Ticker = new SubTicker(ticker, false);

            gameObjectView.OnStart += view => Ticker.Enabled = true;
            gameObjectView.OnEnabled += view => Ticker.Enabled = true;
            gameObjectView.OnDisabled += view => Ticker.Enabled = false;
            gameObjectView.OnDestroyed += view => Ticker.Enabled = false;

            if (gameObjectView.Root.activeInHierarchy)
            {
                Ticker.Enabled = true;
            }
        }
    }
}