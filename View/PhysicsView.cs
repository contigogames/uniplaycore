﻿using UniPlay.Core.GameObject;
using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A <see cref="IPhysicsView"/> of Unitys 3d physics.
    /// </summary>
    public sealed class PhysicsView : IPhysicsView
    {
        private readonly Rigidbody m_rigidbody;

        public PhysicsView(Rigidbody mRigidbody)
        {
            m_rigidbody = mRigidbody;
        }

        public Vector3 Position
        {
            get { return m_rigidbody.position; }
            set { m_rigidbody.position = value; }
        }

        public Quaternion Rotation
        {
            get { return m_rigidbody.rotation; }
            set { m_rigidbody.rotation = value; }
        }

        public Vector3 Velocity
        {
            get { return m_rigidbody.velocity; }
            set { m_rigidbody.velocity = value; }
        }

        public Vector3 AngularVelocity
        {
            get { return m_rigidbody.angularVelocity; }
            set { m_rigidbody.angularVelocity = value; }
        }

        public float Mass
        {
            get { return m_rigidbody.mass; }
        }
    }
}