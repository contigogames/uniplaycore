﻿using System;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A <see cref="ITimerView"/> that will be fired periodically.
    /// </summary>
    public sealed class PeriodicTimerView : ITimerView
    {
        private readonly ITimerView innerTimerView;
        
        public event Action<ITimerView> OnTrigger
        {
            add { innerTimerView.OnTrigger += value; }
            remove { innerTimerView.OnTrigger -= value; }
        }

        public event Action<ITimerView, float> OnTicked
        {
            add { innerTimerView.OnTicked += value; }
            remove { innerTimerView.OnTicked -= value; }
        }

        public PeriodicTimerView(float seconds, ITicker ticker, bool started = false)
        {
            innerTimerView = new OneShotTimerView(seconds, ticker, started);
            innerTimerView.OnTrigger += view =>
            {
                Stop();
                Start();
            };
        }

        public float NormalizedValue
        {
            get { return innerTimerView.NormalizedValue; }
        }

        public void Start()
        {
            innerTimerView.Start();
        }

        public void Pause()
        {
            innerTimerView.Pause();
        }

        public void Stop()
        {
            innerTimerView.Stop();
        }
    }
}