﻿using System;
using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// <see cref="IView"/> to make the object look at something.
    /// </summary>
    public interface ILookAtView : IView
    {
        /// <summary>
        /// Called when the object begins looking.
        /// </summary>
        event Action<ILookAtView> OnStartLookAt;

        /// <summary>
        /// Called when the object finishes looking.
        /// </summary>
        event Action<ILookAtView> OnFinishLookAt;

        Vector3 Origin { get; }

        Vector3 Direction { get; }
        
        bool IsLookingAtTarget { get; }

        void LookAt(Func<Vector3> positionFetcher);

        void LookAtRelative(Func<Vector3> relativePositionFetcher);
    }
}