﻿using System;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A <see cref="ITimerView"/> that will be fired once when the timer runs out until reset.
    /// </summary>
    public sealed class OneShotTimerView : ITimerView
    {
        private readonly float seconds;
        private readonly ITicker ticker;

        private float timer;

        public event Action<ITimerView> OnTrigger;
        public event Action<ITimerView, float> OnTicked;

        public OneShotTimerView(float seconds, ITicker ticker, bool started = false)
        {
            this.seconds = seconds;
            this.ticker = ticker;
            this.timer = 0;

            if (started)
            {
                Start();
            }
        }

        public float NormalizedValue
        {
            get { return timer / seconds; }
        }

        public void Start()
        {
            ticker.OnTick -= OnTick;
            ticker.OnTick += OnTick;
        }

        public void Pause()
        {
            ticker.OnTick -= OnTick;
        }

        public void Stop()
        {
            Pause();
            timer = 0;
        }

        private void OnTick(ITime time)
        {
            timer += time.DeltaTime;
            OnTicked?.Invoke(this, time.DeltaTime);
            if (timer >= seconds)
            {
                Pause();
                OnTrigger?.Invoke(this);
            }
        }
    }
}