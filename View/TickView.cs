﻿using UniPlay.Core.GameObject;
using UniPlay.Core.Ticking;
using UnityEngine;

namespace UniPlay.Core.View
{
    public interface ITickView : IView
    {
        ITicker Ticker { get; }
    }

    /// <summary>
    /// A component for adding a <see cref="ITickView"/>.
    /// </summary>
    public abstract class BaseTickView
        : MonoBehaviour,
            ITickView
    {
        public ITicker Ticker { get; private set;}

        protected abstract ITicker CreateTicker();

        private void Awake()
        {
            Ticker = CreateTicker();
        }

        private void OnEnable()
        {
            SetTickersEnabled(true);
        }

        private void OnDisable()
        {
            SetTickersEnabled(false);
        }

        private void OnDestroy()
        {
            SetTickersEnabled(false);
        }

        private void SetTickersEnabled(bool tickerEnabled)
        {
            Ticker.Enabled = tickerEnabled;
        }
    }
}