using System;
using UniPlay.Core.View;
using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A monobehaviour that wraps some underlying view.
    /// </summary>
    /// <typeparam name="T">The type of view to wrap.</typeparam>
    public abstract class MonoBehaviourViewWrapper<T> : MonoBehaviour
        where T : IView
    {
        private readonly Lazy<T> lazyView;

        protected MonoBehaviourViewWrapper()
        {
            lazyView = new Lazy<T>(CreateView);
        }
        
        protected T View
        {
            get { return lazyView.Value; }
        }
        
        protected abstract T CreateView();
    }
}