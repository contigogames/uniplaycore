﻿using System;
using UniPlay.Core.Ticking;
using UnityEngine;

namespace UniPlay.Core.View
{
    public sealed class GenericTickView : BaseTickView
    {
        private enum TickType
        {
            Normal,
            Late,
            Fixed
        }

        [SerializeField]
        private TickType m_tickType = TickType.Normal;

        [SerializeField]
        private bool m_unscaled = false;

        protected override ITicker CreateTicker()
        {
            ITicker baseTicker;
            switch (m_tickType)
            {
                case TickType.Normal:
                    baseTicker = m_unscaled ? EngineTickers.UnscaledUpdateTicker : EngineTickers.UpdateTicker;
                    break;
                case TickType.Late:
                    baseTicker = m_unscaled ? EngineTickers.LateUnscaledUpdateTicker : EngineTickers.LateUpdateTicker;
                    break;
                case TickType.Fixed:
                    baseTicker = m_unscaled ? EngineTickers.FixedUnscaledUpdateTicker : EngineTickers.FixedUpdateTicker;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return new SubTicker(baseTicker, false);
        }
    }
}