﻿using UnityEngine;

namespace UniPlay.Core.View
{
    [RequireComponent(typeof(Rigidbody))]
    public class PhysicsViewBehaviour : MonoBehaviour, IPhysicsView
    {
        private IPhysicsView physicsViewImplementation;
        
        public Vector3 Position
        {
            get { return physicsViewImplementation.Position; }
            set { physicsViewImplementation.Position = value; }
        }

        public Quaternion Rotation
        {
            get { return physicsViewImplementation.Rotation; }
            set { physicsViewImplementation.Rotation = value; }
        }

        public Vector3 Velocity
        {
            get { return physicsViewImplementation.Velocity; }
            set { physicsViewImplementation.Velocity = value; }
        }

        public Vector3 AngularVelocity
        {
            get { return physicsViewImplementation.AngularVelocity; }
            set { physicsViewImplementation.AngularVelocity = value; }
        }

        public float Mass
        {
            get { return physicsViewImplementation.Mass; }
        }

        private void Start()
        {
            physicsViewImplementation = new PhysicsView(GetComponent<Rigidbody>());
        }
    }
}