﻿using System;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A timer for some event that will be fired on a regular schedule.
    /// </summary>
    public interface ITimerView
    {
        /// <summary>
        /// The event to be fired.
        /// </summary>
        event Action<ITimerView> OnTrigger;

        /// <summary>
        /// Called every time the timer is ticked.
        /// </summary>
        event Action<ITimerView, float> OnTicked; 
        
        /// <summary>
        /// A normalized view of the timer value.
        /// </summary>
        float NormalizedValue { get; }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        void Start();

        /// <summary>
        /// Pauses the timer.
        /// </summary>
        void Pause();

        /// <summary>
        /// Stops the timer. Like pause, but resets the counter.
        /// </summary>
        void Stop();
    }
}