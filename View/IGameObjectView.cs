﻿using System;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A view of an objects <see cref="GameObject"/>.
    /// </summary>
    public interface IGameObjectView : IView
    {
        /// <summary>
        /// Event when the <see cref="GameObject"/> starts up for the first time.
        /// </summary>
        event Action<GameObjectView> OnStart;
        
        /// <summary>
        /// Event when the <see cref="GameObject"/> is enabled.
        /// </summary>
        event Action<GameObjectView> OnEnabled;
        
        /// <summary>
        /// Event when the <see cref="GameObject"/> is disabled.
        /// </summary>
        event Action<GameObjectView> OnDisabled;
        
        /// <summary>
        /// Event when the <see cref="GameObject"/> is destroyed.
        /// </summary>
        event Action<GameObjectView> OnDestroyed;

        /// <summary>
        /// The <see cref="GameObject"/> that is being viewed.
        /// </summary>
        UnityEngine.GameObject Root { get; }

        /// <summary>
        /// Destroys the viewed <see cref="GameObject"/>.
        /// </summary>
        void Destroy();
    }
}