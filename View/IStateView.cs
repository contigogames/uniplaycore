﻿using System;

namespace UniPlay.Core.View
{
    public interface IStateView
    {
        string State { get; }

        event Action<IStateView, string> OnStateChanged;
    }
}