﻿using System;
using UniPlay.Core.GameObject;
using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A view of the objects collision.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class CollisionView
        : MonoBehaviour,
            ICollisionView
    {
        public event Action<Collider> OnCollisionEntered;
        public event Action<Collider> OnCollisionStayed;
        public event Action<Collider> OnCollisionExited;

        [SerializeField] private bool _controlColliderEnabled = true;

        private void OnCollisionEnter(Collision other)
        {
            OnCollisionEntered?.Invoke(other.collider);
        }

        private void OnCollisionStay(Collision other)
        {
            OnCollisionStayed?.Invoke(other.collider);
        }

        private void OnCollisionExit(Collision other)
        {
            OnCollisionExited?.Invoke(other.collider);
        }
        
        private void OnTriggerEnter(Collider other)
        {
            OnCollisionEntered?.Invoke(other);
        }

        private void OnTriggerStay(Collider other)
        {
            OnCollisionStayed?.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            OnCollisionExited?.Invoke(other);
        }

        private void OnEnable() {
            if (!_controlColliderEnabled) {
                return;
            }
            
            var collider = GetComponent<Collider>();
            if (collider) {
                collider.enabled = true;
            }
        }

        private void OnDisable() {
            if (!_controlColliderEnabled) {
                return;
            }
            
            var collider = GetComponent<Collider>();
            if (collider) {
                collider.enabled = false;
            }
        }

        public bool ForceEnable {
            get { return enabled; }
            set {
                enabled = value;
                GetComponent<Collider>().enabled = value;
            }
        }
    }
}