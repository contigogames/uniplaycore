﻿using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// A view for controlling an objects locamotion.
    /// </summary>
    public interface ILocomotionView : IView
    {
        /// <summary>
        /// The direction moving in.
        /// </summary>
        Vector3 Direction { get; }
        
        /// <summary>
        /// The speed of movement.
        /// </summary>
        float Speed { get; }
        
        /// <summary>
        /// The position of the locamotion.
        /// </summary>
        Vector3 Position { get; }

        /// <summary>
        /// Moves the object in a direction.
        /// </summary>
        /// <param name="direction">The direction to move in.</param>
        /// <param name="speed">The speed to move at.</param>
        void MoveInDirection(Vector3 direction, float speed);
    }
}