﻿using UnityEngine;

namespace UniPlay.Core.View
{
    /// <summary>
    /// Implementation of a <see cref="ITransformView"/>.
    /// </summary>
    public sealed  class TransformView : ITransformView
    {
        private readonly Transform m_transform;
        public Vector3 Position
        {
            get { return m_transform.position; }
            set { m_transform.position = value; }
        }

        public Quaternion Rotation
        {
            get { return m_transform.rotation; }
            set { m_transform.rotation = value; }
        }

        public Vector3 Up
        {
            get { return m_transform.up; }
            set { m_transform.up = value; }
        }

        public Vector3 Right
        {
            get { return m_transform.right; }
            set { m_transform.right = value; }
        }

        public Vector3 Forward
        {
            get { return m_transform.forward; }
            set { m_transform.forward = value; }
        }

        public TransformView(Transform transform)
        {
            m_transform = transform;
        }
    }
}