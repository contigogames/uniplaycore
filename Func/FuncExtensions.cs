﻿using System;
using System.Collections.Generic;
using UniPlay.Core.Collection;

namespace UniPlay.Core.Func
{
    public static class FuncExtensions
    {
        public static Func<TResult> Memorize<TResult>(this Func<TResult> func)
        {
            var memory = new Dictionary<Func<TResult>, TResult>();
            return () => memory.InsertIfAbsent(func, func);
        }
        
        public static Func<TIn, TResult> Memorize<TIn, TResult>(this Func<TIn, TResult> func)
        {
            var memory = new Dictionary<TIn, TResult>();
            return input => { return memory.InsertIfAbsent(input, () => func(input)); };
        }

        public static Func<TIn1, TIn2, TResult> Memorize<TIn1, TIn2, TResult>(this Func<TIn1, TIn2, TResult> func)
        {
            var memory = new Dictionary<Tuple<TIn1, TIn2>, TResult>();
            return (in1, in2) => { return memory.InsertIfAbsent(Tuple.Create(in1, in2), () => func(in1, in2)); };
        }

        public static Func<TIn1, TIn2, TIn3, TResult> Memorize<TIn1, TIn2, TIn3, TResult>(
            this Func<TIn1, TIn2, TIn3, TResult> func)
        {
            var memory = new Dictionary<Tuple<TIn1, TIn2, TIn3>, TResult>();
            return (in1, in2, in3) =>
            {
                return memory.InsertIfAbsent(Tuple.Create(in1, in2, in3), () => func(in1, in2, in3));
            };
        }
    }
}