﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UniPlay.Core.Func
{
    public static class CallExt
    {
        /// <summary>
        /// Invokes an action on all targets safely. If any invocation of action throws an exception it will be logged
        /// rather than thrown.
        /// </summary>
        public static void InvokeSafe<T>(this IEnumerable<T> targets, Action<T> action)
        {
            foreach (var target in targets)
            {
                try
                {
                    action.Invoke(target);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }
}