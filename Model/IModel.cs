﻿using UniPlay.Core.Actor;

namespace UniPlay.Core.Model
{
    /// <summary>
    /// A piece of data about a <see cref="IActor"/>.
    /// </summary>
    public interface IModel { }
}