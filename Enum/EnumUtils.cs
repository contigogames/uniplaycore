﻿using System;

namespace UniPlay.Core.Enum
{
    /// <summary>
    /// Provides tools for dealing with enums.
    /// </summary>
    public static class EnumUtils
    {
        /// <summary>
        /// Parses an enum from a stirng.
        /// </summary>
        /// <param name="str">The string to parse.</param>
        /// <typeparam name="T">The type of enumn to parse.</typeparam>
        /// <returns>The parsed enum, or the default value.</returns>
        public static T FromString<T>(string str)
        {
            if (System.Enum.IsDefined(typeof(T), str))
            {
                return default(T);
            }

            return (T) System.Enum.Parse(typeof(T), str);
        }

        /// <summary>
        /// Checks if an enum contains a flag.
        /// </summary>
        /// <param name="value">The enum value to check for a flag.</param>
        /// <param name="flag">The enum flag to check for.</param>
        /// <typeparam name="T">The type of enum to use.</typeparam>
        /// <returns>True if the flag is present in the value.</returns>
        public static bool HasFlag<T>(T value, T flag)
        {
            var intValue = Convert.ToUInt32(value);
            var intFlag = Convert.ToUInt32(flag);
            return (intValue & intFlag) == intFlag;
        }
    }
}
