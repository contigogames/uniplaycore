﻿using UnityEngine;

namespace UniPlay.Core.Actor
{
    public abstract class ActorCreator : ActorBinder
    {
        protected abstract IActor CreateActor();
        
        protected sealed override IActorBinding CreateActorBinding()
        {
            return new Binding()
            {
                Obj = gameObject,
                Actor = CreateActor()
            };
        }
        
        private class Binding : IActorBinding
        {
            public IActor Actor { get; set; }
            public Object Obj { get; set; }
        }
    }
}