﻿using System;
using System.Collections;
using UnityEngine;

namespace UniPlay.Core.Actor {
    [DefaultExecutionOrder(int.MinValue + 100)]
    public abstract class ActorBinder : MonoBehaviour {
        public IActorBinding CurrentBinding { get; private set; }

        public readonly StartupSignalImpl StartupSignal = new StartupSignalImpl();

        public readonly StartupSignalImpl InternalStartupSignal = new StartupSignalImpl();

        public event Action<ActorBinder> ResetSignal;
        public event Action<ActorBinder> InternalResetSignal;

        public event Action<ActorBinder> StartupOrResetSignal {
            add {
                StartupSignal.OnStartup += value;
                ResetSignal += value;
            }
            remove {
                StartupSignal.OnStartup -= value;
                ResetSignal -= value;
            }
        }

        public event Action<ActorBinder> InternalStartupOrResetSignal {
            add {
                InternalStartupSignal.OnStartup += value;
                InternalResetSignal += value;
            }
            remove {
                InternalStartupSignal.OnStartup -= value;
                InternalResetSignal -= value;
            }
        }

        public event Action<ActorBinder> ShutdownSignal;

        private bool _hasCalledStart;

        protected abstract IActorBinding CreateActorBinding();

        private void Awake() {
            CurrentBinding = CreateActorBinding();
        }

        public void AddToPlay() {
            if (!_hasCalledStart) {
                gameObject.SetActive(true);
                return;
            }

            GlobalCoroutineRunner.RunCoroutine(ResetRoutine());
        }

        private void OnDisable() {
            ShutdownSignal?.Invoke(this);
        }

        private IEnumerator ResetRoutine() {
            yield return null;
            gameObject.SetActive(true);
            ResetSignal?.Invoke(this);
            InternalResetSignal?.Invoke(this);
        }

        private void Start() {
            StartupSignal.SendStartupSignal(this);
            InternalStartupSignal.SendStartupSignal(this);
            _hasCalledStart = true;
        }
    }
}
