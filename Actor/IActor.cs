﻿using System;
using System.Collections.Generic;
using UniPlay.Core.Model;
using UniPlay.Core.View;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// Represents an object composed of a collection of pieces.
    /// </summary>
    public interface IActor
    {
        ICollection<IView> Views { get; }

        ICollection<IModel> Models { get; }

        T GetView<T>()
            where T : class, IView;

        IView GetView(Type type);

        T GetModel<T>()
            where T : class, IModel;

        IModel GetModel(Type type);
    }
}