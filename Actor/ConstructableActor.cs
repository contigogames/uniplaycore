﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniPlay.Core.Logic;
using UniPlay.Core.Model;
using UniPlay.Core.View;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// An <see cref="IActor"/> that can be constructed.
    /// </summary>
    public class ConstructableActor : IActor
    {
        private readonly ICollection<IView> m_views;
        private readonly ICollection<IModel> m_models;

        public ICollection<IView> Views
        {
            get { return m_views; }
        }

        public ICollection<IModel> Models
        {
            get { return m_models; }
        }

        public ConstructableActor(ICollection<IView> views, ICollection<IModel> models, ICollection<ILogic> logic)
        {
            m_views = views;
            m_models = models;
        }

        public ConstructableActor(ICollection<object> pieces)
        {
            m_views = pieces.OfType<IView>().ToList();
            m_models = pieces.OfType<IModel>().ToList();
        }

        public T GetView<T>()
            where T : class, IView {
            return m_views.OfType<T>().FirstOrDefault();
        }

        public IView GetView(Type type)
        {
            return m_views.Where(type.IsInstanceOfType).FirstOrDefault();
        }

        public T GetModel<T>()
            where T : class, IModel {
            return m_models.OfType<T>().FirstOrDefault();
        }

        public IModel GetModel(Type type)
        {
            return m_models.Where(type.IsInstanceOfType).FirstOrDefault();
        }
    }
}