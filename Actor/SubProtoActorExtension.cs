using System;
using System.Linq;
using UnityEngine;

namespace UniPlay.Core.Actor {
    public class SubProtoActorExtension : ProtoActorExtension {
        [SerializeField] private ProtoActorExtension[] _subExtensions;
        
        public override void ExtendActor(IActorProto actorProto) {
            foreach (var subExtension in _subExtensions.OrderBy(extension => extension.Order)) {
                subExtension.ExtendActor(actorProto);
            }
        }
    }
}