﻿using UnityEngine;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// Behaviour that will extend the actor proto.
    /// </summary>
    public abstract class ProtoActorExtension : MonoBehaviour
    {
        public abstract void ExtendActor(IActorProto actorProto);

        public virtual int Order => 0;
    }
}