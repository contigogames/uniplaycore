﻿using System;
using System.Collections.Generic;
using UniPlay.Core.Collection;
using UniPlay.Core.Model;
using UniPlay.Core.Ticking;
using UniPlay.Core.View;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// Prototype for creating an actor.
    /// </summary>
    public class ActorProto : IActorProto
    {
        private readonly IDictionary<Type, IView> views = new Dictionary<Type, IView>();
        
        private readonly IDictionary<Type, IModel> models = new Dictionary<Type, IModel>();
        
        public ICollection<IView> Views
        {
            get { return new HashSet<IView>(views.Values); }
        }

        public ICollection<IModel> Models
        {
            get { return new HashSet<IModel>(models.Values); }
        }

        public T GetView<T>()
            where T : class, IView {
            return (T) GetView(typeof(T));
        }

        public IView GetView(Type type) {
            return views.GetOrDefault(type);
        }

        public T GetModel<T>()
            where T : class, IModel {
            return (T) GetModel(typeof(T));
        }

        public IModel GetModel(Type type)
        {
            return models.GetOrDefault(type);
        }

        public void AddView(IView view, Type bindingType)
        {
            views.Add(bindingType, view);
        }

        public void AddModel(IModel model, Type bindingType)
        {
            models.Add(bindingType, model);
        }
    }
}