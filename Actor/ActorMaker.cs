﻿using System.Collections.Generic;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// <see cref="ActorCreator"/> that already has a maker.
    /// </summary>
    public abstract class ActorMaker : ActorCreator
    {
        protected override IActor CreateActor()
        {
            return new Actor(Maker());
        }

        protected abstract IEnumerable<object> Maker();
    }
}