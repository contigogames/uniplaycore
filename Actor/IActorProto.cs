﻿using System;
using System.Linq;
using UniPlay.Core.Model;
using UniPlay.Core.View;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// Defines an actor that is still being built.
    /// </summary>
    public interface IActorProto : IActor
    {
        void AddView(IView view, Type bindingType);

        void AddModel(IModel model, Type bindingType);
    }

    public static class ActorProtoExt
    {
        private static T AddDirect<T>(T obj, Action<T, Type> addingMethod)
        {
            var type = obj.GetType();
            addingMethod(obj, type);

            var allInterfaces = type.GetInterfaces();
            var minimalInterfaces = allInterfaces.Except(
                allInterfaces.SelectMany(t => t.GetInterfaces())
                    .Concat(
                        new[]
                        {
                            typeof(T)
                        }));

            foreach (var inter in minimalInterfaces)
            {
                addingMethod(obj, inter);
            }

            return obj;
        }

        /// <summary>
        /// Adds a view to the actor under all interfaces that are directly related to the view type.
        /// </summary>
        /// <returns>The added view.</returns>
        public static T AddViewDirect<T>(this IActorProto actorProto, T view) where T : class, IView 
        {
            return (T) AddDirect<IView>(view, actorProto.AddView);
        }

        /// <summary>
        /// Adds a models to the actor under all interfaces that are directly related to the model type.
        /// </summary>
        /// <returns>The added model.</returns>
        public static T AddModelDirect<T>(this IActorProto actorProto, T model) where T : class, IModel
        {
            return (T) AddDirect<IModel>(model, actorProto.AddModel);
        }

        public static T GetOrCreateView<T>(this IActorProto actorProto, Func<T> creator) where T : class, IView {
            var found = actorProto.GetView<T>();
            if (found != null) {
                return found;
            }

            return actorProto.AddViewDirect(creator());
        }
        
        public static T GetOrCreateModel<T>(this IActorProto actorProto, Func<T> creator) where T : class, IModel {
            var found = actorProto.GetModel<T>();
            if (found != null) {
                return found;
            }

            return actorProto.AddModelDirect(creator());
        }
    }
}