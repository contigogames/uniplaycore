﻿using System.Linq;
using UniPlay.Core.Collection;

namespace UniPlay.Core.Actor {
    /// <summary>
    /// <see cref="ActorBinder"/> that uses a <see cref="IActorProto"/>.
    /// </summary>
    public abstract class ProtoActorMaker : ActorBinder {
        protected override IActorBinding CreateActorBinding() {
            var proto = CreateActorProto();
            GetComponents<ProtoActorExtension>()
                .OrderBy(extension => extension.Order)
                .Call(extension => extension.ExtendActor(proto));

            return new Binding() {
                Actor = proto,
                Obj = gameObject
            };
        }

        protected abstract IActorProto CreateActorProto();

        private class Binding : IActorBinding {
            public IActor Actor { get; set; }
            public UnityEngine.Object Obj { get; set; }
        }
    }
}
