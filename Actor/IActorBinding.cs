﻿using UniPlay.Core.Model;
using UniPlay.Core.View;
using UnityEngine;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// A binding between an <see cref="IActor"/> and <see cref="Object"/>.
    /// </summary>
    public interface IActorBinding
    {
        IActor Actor { get; }

        Object Obj { get; }
    }
}