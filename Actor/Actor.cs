﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// A form of <see cref="ConstructableActor"/> that is constructed using a func.
    /// </summary>
    public class Actor : ConstructableActor
    {
        public Actor(IEnumerable<object> maker) : base(maker.ToArray()) { }
    }
}