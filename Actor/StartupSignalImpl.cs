﻿using System;
using UniPlay.Core.Signal;

namespace UniPlay.Core.Actor
{
    /// <summary>
    /// A signal sent on startup of an <see cref="IActor"/>. This is generally fired right after an actor is created.
    /// </summary>
    public interface IStartupSignal : ISignal
    {
        event Action<ActorBinder> OnStartup;
    }

    /// <summary>
    /// Sends a startup signal.
    /// </summary>
    public interface IStartupSignalSender : ISignalSender
    {
        void SendStartupSignal(ActorBinder binder);
    }

    /// <summary>
    /// Implementation of the startup signal.
    /// </summary>
    public class StartupSignalImpl
        : IStartupSignal,
            IStartupSignalSender
    {
        private Action<ActorBinder> onStartup;

        private ActorBinder lastInstigator;
        
        public event Action<ActorBinder> OnStartup
        {
            add
            {
                onStartup += value;
                if (lastInstigator != null)
                {
                    value?.Invoke(lastInstigator);
                }
            }
            remove { onStartup -= value; }
        }

        public void SendStartupSignal(ActorBinder binder)
        {
            lastInstigator = binder;
            onStartup?.Invoke(binder);
        }
    }
}