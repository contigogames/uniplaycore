﻿using UniPlay.Core.Actor;

namespace UniPlay.Core.Logic
{
    /// <summary>
    /// A piece of logic to run as a part of a <see cref="IActor"/>.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// If the logic is enabled or not.
        /// </summary>
        bool Enabled { get; set; }
    }
}