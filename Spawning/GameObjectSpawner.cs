﻿using System.Collections;
using UniPlay.Core.GameObject;
using UniPlay.Core.Tasks;
using UnityEngine;

namespace UniPlay.Core.Spawning
{
    /// <summary>
    /// <see cref="ISpawner"/> that will spawn a <see cref="GameObject"/>.
    /// </summary>
    public class GameObjectSpawner : ISpawner<UnityEngine.GameObject>
    {
        public static readonly GameObjectSpawner Default = new GameObjectSpawner();
        
        public Promise<UnityEngine.GameObject> Spawn(Object prefab)
        {
            var newObject = Object.Instantiate(prefab);
            var callbackBehaviour = newObject.GetGameObject().AddComponent<SpawnCallbackBehaviour>();
            callbackBehaviour.Promise = new Promise<UnityEngine.GameObject>();

            return callbackBehaviour.Promise;
        }

        /// <summary>
        /// Special component to add that will callback the promise on start.
        /// </summary>
        private class SpawnCallbackBehaviour : MonoBehaviour
        {
            public Promise<UnityEngine.GameObject> Promise;
            
            private void Awake()
            {
                hideFlags = HideFlags.DontSave | HideFlags.HideInInspector;
            }

            private void Start()
            {
                Promise?.Fullfill(gameObject);
            }
        }
    }
}