﻿using UniPlay.Core.Tasks;
using UnityEngine;

namespace UniPlay.Core.Spawning
{
    /// <summary>
    /// Class for spawning objects.
    /// </summary>
    /// <typeparam name="T">The type of object to spawn.</typeparam>
    public interface ISpawner<T>
    {
        /// <summary>
        /// Spawns an object from a prefab.
        /// </summary>
        /// <param name="prefab">The prefab to spawn from.</param>
        /// <returns>A promise to be fullfilled when the object is spawned.</returns>
        Promise<T> Spawn(Object prefab);
    }
}