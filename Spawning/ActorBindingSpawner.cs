﻿using System;
using UniPlay.Core.Actor;
using UniPlay.Core.GameObject;
using UniPlay.Core.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UniPlay.Core.Spawning
{
    /// <summary>
    /// <see cref="ISpawner"/> that will spawn a <see cref="IActor"/>.
    /// </summary>
    public class ActorBindingSpawner : ISpawner<ActorBinder>
    {
        public static readonly ActorBindingSpawner Default = new ActorBindingSpawner();
        
        public Promise<ActorBinder> Spawn(Object prefab)
        {
            var newObject = Object.Instantiate(prefab);

            var actorBinding = newObject.GetBinder();
            if (actorBinding != null)
            {
                var promise = new Promise<ActorBinder>();
                actorBinding.StartupSignal.OnStartup += binder => promise.Fullfill(binder);

                return promise;
            }
            else
            {
                throw new InvalidOperationException("No actor binding was provided on the given prefab.");
            }
        }
    }
}