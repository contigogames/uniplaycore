﻿using System;
using UniPlay.Core.Time;

namespace UniPlay.Core.Ticking
{
    /// <summary>
    /// A ticker that ticks as a part of a parent ticker. This ticker will only register to the parent when delegates
    /// exist. If no delegates exist the the ticker will un-register to remove overhead.
    /// </summary>
    public sealed class SubTicker : ITicker
    {
        private readonly ITicker m_masterTicker;
        private readonly ExternalTicker m_impl = new ExternalTicker();

        private bool m_isRegisteredToMaster;

        public SubTicker(ITicker masterTicker, bool startEnabled = true)
        {
            m_masterTicker = masterTicker;

            m_impl.OnDelegatesChanged += ImplOnOnDelegatesChanged;
            m_impl.OnEnabledChanged += ImplOnOnEnabledChanged;

            m_impl.Enabled = startEnabled;
        }

        public void AddDelegate(OnTickDelegate tickDelegate)
        {
            m_impl.AddDelegate(tickDelegate);
        }

        public void RemoveDelegate(OnTickDelegate tickDelegate)
        {
            m_impl.RemoveDelegate(tickDelegate);
        }

        public event OnTickDelegate OnTick
        {
            add { m_impl.OnTick += value; }
            remove { m_impl.OnTick -= value; }
        }

        public bool Enabled
        {
            get { return m_impl.Enabled; }
            set { m_impl.Enabled = value; }
        }

        private void ImplOnOnDelegatesChanged(ExternalTicker externalTicker)
        {
            if (Enabled)
            {
                if (externalTicker.HasRegisteredDelegates && !m_isRegisteredToMaster)
                {
                    m_isRegisteredToMaster = true;
                    m_masterTicker.OnTick += TickDelegate;
                }
                else if (!externalTicker.HasRegisteredDelegates && m_isRegisteredToMaster)
                {
                    m_isRegisteredToMaster = false;
                    m_masterTicker.OnTick -= TickDelegate;
                }
            }
        }

        private void ImplOnOnEnabledChanged(ExternalTicker externalTicker)
        {
            if (Enabled && externalTicker.HasRegisteredDelegates && !m_isRegisteredToMaster)
            {
                m_isRegisteredToMaster = true;
                m_masterTicker.OnTick += TickDelegate;
            }
            else if (!Enabled && m_isRegisteredToMaster)
            {
                m_isRegisteredToMaster = false;
                m_masterTicker.OnTick -= TickDelegate;
            }
        }

        private void TickDelegate(ITime time)
        {
            m_impl.Tick(time);
        }
    }
}