﻿using System;
using UniPlay.Core.Signal;
using UniPlay.Core.Time;

namespace UniPlay.Core.Ticking
{
    public delegate void OnTickDelegate(ITime time);
    
    /// <summary>
    /// An object that will signal out on a game tick event.
    /// </summary>
    public interface ITicker : ISignal
    {
        /// <summary>
        /// Adds a delegate to be called on tick.
        /// </summary>
        [Obsolete]
        void AddDelegate(OnTickDelegate tickDelegate);

        /// <summary>
        /// Removes a delegate to be called on tick.
        /// </summary>
        [Obsolete]
        void RemoveDelegate(OnTickDelegate tickDelegate);

        event OnTickDelegate OnTick; 
        
        /// <summary>
        /// If the ticker is active or not.
        /// </summary>
        bool Enabled { get; set; }
    }
}