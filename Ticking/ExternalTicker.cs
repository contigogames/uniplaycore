﻿using System;
using System.Collections.Generic;
using System.Threading;
using UniPlay.Core.Signal;
using UniPlay.Core.Time;

namespace UniPlay.Core.Ticking
{
    /// <summary>
    /// <see cref="ITicker"/> that is told to tick by an external source. Good to use as the base of ticking logic.
    /// </summary>
    public sealed class ExternalTicker
        : ITicker,
            ISignalSender
    {
        private readonly ICollection<OnTickDelegate> m_tickDelegates = new HashSet<OnTickDelegate>();
        private readonly ICollection<OnTickDelegate> m_waitingForAdd = new HashSet<OnTickDelegate>();
        private readonly ICollection<OnTickDelegate> m_waitingForRemoval = new HashSet<OnTickDelegate>();

        public bool HasRegisteredDelegates
        {
            get { return m_tickDelegates.Count > 0 || m_waitingForAdd.Count > 0; }
        }

        public event Action<ExternalTicker> OnDelegatesChanged;
        public event Action<ExternalTicker> OnEnabledChanged;

        private bool m_isTicking;
        private bool m_enabled;

        public void AddDelegate(OnTickDelegate tickDelegate)
        {
            OnTick += tickDelegate;
        }

        public void RemoveDelegate(OnTickDelegate tickDelegate)
        {
            OnTick -= tickDelegate;
        }

        public event OnTickDelegate OnTick
        {
            add
            {
                if (m_isTicking)
                {
                    m_waitingForAdd.Add(value);
                    m_waitingForRemoval.Remove(value);
                }
                else
                {
                    m_tickDelegates.Add(value);
                    OnDelegatesChanged?.Invoke(this);
                }
            }
            remove
            {
                if (m_isTicking)
                {
                    m_waitingForRemoval.Add(value);
                    m_waitingForAdd.Remove(value);
                }
                else
                {
                    m_tickDelegates.Remove(value);
                    OnDelegatesChanged?.Invoke(this);
                }
            }
        }

        public bool Enabled
        {
            get { return m_enabled; }
            set
            {
                var isDiff = m_enabled != value;
                m_enabled = value;

                if (isDiff)
                {
                    OnEnabledChanged?.Invoke(this);
                }
            }
        }

        /// <summary>
        /// Will invoke a tick event.
        /// </summary>
        public void Tick(ITime time)
        {
            if (Enabled)
            {
                m_isTicking = true;

                foreach (var onTickDelegate in m_tickDelegates)
                {
                    onTickDelegate?.Invoke(time);
                }

                m_isTicking = false;

                var wasChanged = m_waitingForAdd.Count > 0 || m_waitingForRemoval.Count > 0;

                foreach (var tickDelegate in m_waitingForAdd)
                {
                    m_tickDelegates.Add(tickDelegate);
                }

                m_waitingForAdd.Clear();

                foreach (var tickDelegate in m_waitingForRemoval)
                {
                    m_tickDelegates.Remove(tickDelegate);
                }

                m_waitingForRemoval.Clear();

                if (wasChanged)
                {
                    OnDelegatesChanged?.Invoke(this);
                }
            }
        }
    }
}