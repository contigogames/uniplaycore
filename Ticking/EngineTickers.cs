﻿using UniPlay.Core.Engine;
using UniPlay.Core.Engine.Startup;
using UniPlay.Core.Time;
using UnityEngine;

namespace UniPlay.Core.Ticking
{
    /// <summary>
    /// Common tickers that are managed by the engine.
    /// </summary>
    public static class EngineTickers
    {
        /// <summary>
        /// A ticker that is called oncer per update loop.
        /// </summary>
        public static ExternalTicker UpdateTicker { get; private set; }
        
        /// <summary>
        /// A ticker that is called once per update loop with unscaled time.
        /// </summary>
        public static ExternalTicker UnscaledUpdateTicker { get; private set; }

        /// <summary>
        /// A ticker that is called late once per update loop.
        /// </summary>
        public static ExternalTicker LateUpdateTicker { get; private set; }
        
        /// <summary>
        /// A ticker that is called late once per update loop with unscaled time.
        /// </summary>
        public static ExternalTicker LateUnscaledUpdateTicker { get; private set; }

        /// <summary>
        /// A ticker that is called oncer per physics update.
        /// </summary>
        public static ExternalTicker FixedUpdateTicker { get; private set; }
        
        /// <summary>
        /// A ticker that is called oncer per physics update with unscaled time.
        /// </summary>
        public static ExternalTicker FixedUnscaledUpdateTicker { get; private set; }

        [StartupSystem(int.MinValue)]
        private static void Startup()
        {
            UpdateTicker = new ExternalTicker
            {
                Enabled = true
            };

            UnscaledUpdateTicker = new ExternalTicker()
            {
                Enabled = true
            };

            LateUpdateTicker = new ExternalTicker()
            {
                Enabled = true
            };

            LateUnscaledUpdateTicker = new ExternalTicker()
            {
                Enabled = true
            };

            FixedUpdateTicker = new ExternalTicker()
            {
                Enabled = true
            };

            FixedUnscaledUpdateTicker = new ExternalTicker()
            {
                Enabled = true
            };

            var go = new UnityEngine.GameObject("Ticking")
            {
                hideFlags = HideFlags.DontSaveInEditor | HideFlags.HideInHierarchy
            };
            Object.DontDestroyOnLoad(go);

            go.AddComponent<Handler>();
        }

        private class Handler : MonoBehaviour
        {
            private void Awake()
            {
                hideFlags = HideFlags.HideInInspector;
            }

            private void Update()
            {
                UpdateTicker.Tick(EngineTimes.ScaledTime);
                UnscaledUpdateTicker.Tick(EngineTimes.UnscaledTime);
            }

            private void LateUpdate()
            {
                LateUpdateTicker.Tick(EngineTimes.ScaledTime);
                LateUnscaledUpdateTicker.Tick(EngineTimes.UnscaledTime);
            }

            private void FixedUpdate()
            {
                FixedUpdateTicker.Tick(EngineTimes.FixedScaledTime);
                FixedUnscaledUpdateTicker.Tick(EngineTimes.FixedUnscaledTime);
            }
        }
    }
}