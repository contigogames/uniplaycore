﻿using System;
using UniPlay.Core.Func;
using UniPlay.Core.Ticking;
using UniPlay.Core.Time;

namespace UniPlay.Core.Actions
{
    public static class ActionExt
    {
        public static void SafeInvoke<T1>(this Action<T1> action, T1 param1)
        {
            action.GetInvocationList().InvokeSafe(d => ((Action<T1>) d).Invoke(param1));
        }
        
        public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 param1, T2 param2)
        {
            action.GetInvocationList().InvokeSafe(d => ((Action<T1, T2>) d).Invoke(param1, param2));
        }
        
        public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3)
        {
            action.GetInvocationList().InvokeSafe(d => ((Action<T1, T2, T3>) d).Invoke(param1, param2, param3));
        }

        public static void SafeInvoke(this OnTickDelegate onTickDelegate, ITime time)
        {
            onTickDelegate.GetInvocationList().InvokeSafe(tickDelegate => ((OnTickDelegate) tickDelegate).Invoke(time));
        }

        public static void SafeInvoke(this Delegate d, params object[] paramaters)
        {
            d.GetInvocationList().InvokeSafe(target => target.DynamicInvoke(paramaters));
        }
    }
}