using System.Collections.Generic;

namespace UniPlay.Core.Collection
{
    public interface IReadOnlyFlexibleList<T> : IReadOnlyList<T>
    {
        IReadOnlyFlexibleListNode<T> First { get; }
        IReadOnlyFlexibleListNode<T> Last { get; }
        IReadOnlyFlexibleListNode<T> FirstWithValue(T value);
    }

    public interface IReadOnlyFlexibleListNode<T>
    {
        T Value { get; }
        IReadOnlyFlexibleListNode<T> Prev { get; }
        IReadOnlyFlexibleListNode<T> Next { get; }
    }
}