﻿using System;
using System.Collections.Generic;

namespace UniPlay.Core.Collection
{
    public static class DictionaryExtension
    {
        /// <summary>
        /// Gets the value of a key in a dictionary, or inserts a value if the key doesn't already exist.
        /// </summary>
        public static TValue InsertIfAbsent<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            Func<TValue> valueCreator)
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                value = valueCreator();
                dictionary.Add(key, value);
            }

            return value;
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue value;
            if (dictionary.TryGetValue(key, out value))
            {
                return value;
            }

            return default(TValue);
        }
    }
}