﻿using System;
using System.Collections.Generic;

namespace UniPlay.Core.Collection
{
    public static class EnumerableHelper
    {
        /// <summary>
        /// Call a function on all elements.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="func">The function to call on all members.</param>
        /// <typeparam name="T">The type of enumerable.</typeparam>
        /// <typeparam name="R">The type of return type.</typeparam>
        /// <returns>An enumerable of results.</returns>
        public static void Call<T>(this IEnumerable<T> self, Action<T> func)
        {
            foreach (var o in self)
            {
                func(o);
            }
        }
        
        /// <summary>
        /// Call a function on all elements.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="func">The function to call on all members.</param>
        /// <typeparam name="T">The type of enumerable.</typeparam>
        /// <typeparam name="R">The type of return type.</typeparam>
        /// <returns>An enumerable of results.</returns>
        public static IEnumerable<R> Call<T, R>(this IEnumerable<T> self, Func<T, R> func)
        {
            foreach (var o in self)
            {
                yield return func(o);
            }
        }

        /// <summary>
        /// Try calling a function on all members until one returns false.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="func">The function to try calling on all members.</param>
        /// <typeparam name="T">The type of enumerable.</typeparam>
        /// <returns>True if all members returned true, false otherwise.</returns>
        public static bool Try<T>(this IEnumerable<T> self, Func<T, bool> func)
        {
            foreach (var o in self)
            {
                if (!func(o))
                {
                    return false;
                }
            }

            return true;
        }

        public static ICollection<T> CopyTo<T>(this IEnumerable<T> src, ICollection<T> dst) {
            foreach (var v in src) {
                dst.Add(v);
            }

            return dst;
        }
    }
}