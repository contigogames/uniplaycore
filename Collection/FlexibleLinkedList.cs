using System.Collections.Generic;
using UnityEngine.Assertions;

namespace UniPlay.Core.Collection
{
    public partial class FlexibleLinkedList<T> : IReadOnlyFlexibleList<T>, ICollection<T>
    {
        private int m_count;
        
        public Node First { get; private set; }
        public Node Last { get; private set; }

        IReadOnlyFlexibleListNode<T> IReadOnlyFlexibleList<T>.First
        {
            get { return First; }
        }

        IReadOnlyFlexibleListNode<T> IReadOnlyFlexibleList<T>.Last
        {
            get { return Last; }
        }

        public FlexibleLinkedList() { }

        public FlexibleLinkedList(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                AddLast(item);
            }
        }

        public Node AddLast(T value)
        {
            var node = new Node(value);
            node.AddLast(this);

            return node;
        }

        public Node AddFirst(T value)
        {
            var node = new Node(value);
            node.AddFirst(this);

            return node;
        }

        public Node FirstWithValue(T value)
        {
            for (var node = First; node != null; node = node.Next)
            {
                if (Equals(node.Value, value))
                {
                    return node;
                }
            }

            return null;
        }

        IReadOnlyFlexibleListNode<T> IReadOnlyFlexibleList<T>.FirstWithValue(T value)
        {
            return FirstWithValue(value);
        }

        private void OnNodeAdded(Node node)
        {
            m_count++;
        }

        private void OnNodeRemoved(Node node)
        {
            m_count--;
        }

        public class Node : IReadOnlyFlexibleListNode<T>
        {
            public T Value { get; }
            public Node Prev { get; private set; }
            public Node Next { get; private set; }
            public FlexibleLinkedList<T> List { get; private set; }
            
            IReadOnlyFlexibleListNode<T> IReadOnlyFlexibleListNode<T>.Prev
            {
                get { return Prev; }
            }
            
            IReadOnlyFlexibleListNode<T> IReadOnlyFlexibleListNode<T>.Next
            {
                get { return Next; }
            }

            public bool IsInList
            {
                get { return List != null; }
            }

            public bool IsFirst
            {
                get { return IsInList && Prev == null; }
            }

            public bool IsLast
            {
                get { return IsInList && Next == null; }
            }

            public Node(T value)
            {
                Value = value;
            }

            public void AddBefore(Node node)
            {
                Assert.IsNotNull(node, "node != null");

                RemoveFromCurrentList();

                List = node.List;

                var prev = node.Next;
                var next = node;

                DirectSetPrev(prev);
                DirectSetNext(next);

                prev?.DirectSetNext(this);
                next?.DirectSetPrev(this);
                
                AddToCurrentList();
            }

            public void AddAfter(Node node)
            {
                Assert.IsNotNull(node, "node != null");

                RemoveFromCurrentList();

                List = node.List;

                var prev = node;
                var next = node.Next;

                DirectSetPrev(prev);
                DirectSetNext(next);

                prev?.DirectSetNext(this);
                next?.DirectSetPrev(this);
                
                AddToCurrentList();
            }

            public void AddFirst(FlexibleLinkedList<T> list)
            {
                RemoveFromCurrentList();

                if (list.First != null)
                {
                    list.First.AddBefore(this);
                }
                else
                {
                    List = list;
                    AddToCurrentList();
                }
            }

            public void AddLast(FlexibleLinkedList<T> list)
            {
                RemoveFromCurrentList();

                if (list.Last != null)
                {
                    list.Last.AddAfter(this);
                }
                else
                {
                    List = list;
                    AddToCurrentList();
                }
            }

            public void Remove()
            {
                RemoveFromCurrentList();
            }

            private void RemoveFromCurrentList()
            {
                if (IsInList)
                {
                    if (IsFirst)
                    {
                        List.First = Next;
                    }

                    if (IsLast)
                    {
                        List.Last = Prev;
                    }

                    Prev?.DirectSetNext(Next);
                    Next?.DirectSetPrev(Prev);

                    List.OnNodeRemoved(this);
                    
                    Next = null;
                    Prev = null;
                    List = null;
                }
            }

            private void AddToCurrentList()
            {
                if (IsFirst)
                {
                    List.First = this;
                }

                if (IsLast)
                {
                    List.Last = this;
                }

                List.OnNodeAdded(this);
            }

            private void DirectSetPrev(Node prev)
            {
                Prev = prev;
            }

            private void DirectSetNext(Node next)
            {
                Next = next;
            }

            private void DirectSetList(FlexibleLinkedList<T> list)
            {
                List = list;
            }
        }
    }
}