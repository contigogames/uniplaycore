using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UniPlay.Core.Collection
{
    public partial class FlexibleLinkedList<T>
    {
        public IEnumerator<T> GetEnumerator()
        {
            for (var node = First; node != null; node = node.Next)
            {
                if (!node.IsInList || node.List != this)
                {
                    throw new AccessViolationException();
                }

                yield return node.Value;
            }
        }

        public IEnumerable<T> GetSafeEnumeration()
        {
            for (var node = First; node != null;)
            {
                if (!node.IsInList || node.List != this)
                {
                    throw new AccessViolationException();
                }

                var next = node.Next;
                yield return node.Value;
                node = next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>) this).GetEnumerator();
        }

        void ICollection<T>.Add(T item)
        {
            AddLast(item);
        }

        public void Clear()
        {
            Remove(_ => true);
        }

        public bool Contains(T item)
        {
            return this.Any(v => Equals(v, item));
        }

        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(arrayIndex),
                    (object) arrayIndex,
                    "Non-negative number required.");
            }

            if (arrayIndex > array.Length)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(arrayIndex),
                    (object) arrayIndex,
                    "Must be less than or equal to the size of the collection.");
            }

            if (array.Length - arrayIndex < Count)
            {
                throw new ArgumentException("Insufficient space in the target location to copy the information.");
            }

            var linkedListNode = First;
            if (linkedListNode == null)
            {
                return;
            }

            do
            {
                array[arrayIndex++] = linkedListNode.Value;
                linkedListNode = linkedListNode.Next;
            } while (linkedListNode != Last);
        }

        public bool Remove(T item)
        {
            return Remove(obj => Equals(obj, item));
        }

        public bool Remove(Predicate<T> predicate)
        {
            var didRemove = false;
            for (var node = First; node != null; node = node.Next)
            {
                if (predicate(node.Value))
                {
                    didRemove = true;
                    node.Remove();
                }
            }

            return didRemove;
        }

        public int Count
        {
            get { return m_count; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { return false; }
        }

        public int IndexOf(T item)
        {
            var i = 0;
            for (var node = First; node != null; node = node.Next)
            {
                if (Equals(node.Value, item))
                {
                    return i;
                }

                i++;
            }

            return -1;
        }

        public void Insert(int index, T item)
        {
            GetNodeAtIndex(index).AddBefore(new Node(item));
        }

        public void RemoveAt(int index)
        {
            GetNodeAtIndex(index).Remove();
        }

        public T this[int index]
        {
            get { return GetNodeAtIndex(index).Value; }
        }

        private Node GetNodeAtIndex(int index)
        {
            var i = 0;
            for (var node = First; node != null; node = node.Next)
            {
                if (i == index)
                {
                    return node;
                }

                i++;
            }

            throw new IndexOutOfRangeException();
        }
    }
}