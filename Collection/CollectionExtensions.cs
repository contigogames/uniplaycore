﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace UniPlay.Core.Collection
{
    /// <summary>
    /// Provides extension methods for any <see cref="ICollection{T}"/>
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Checks to see if a collection is empty.
        /// </summary>
        /// <param name="self">The collection to check.</param>
        /// <typeparam name="T">The type of item in the collection.</typeparam>
        /// <returns>True if the collection is empty.</returns>
        public static bool IsEmpty<T>(this ICollection<T> self)
        {
            return self.Count == 0;
        }

        /// <summary>
        /// Checks to see if a collection is not empty.
        /// </summary>
        /// <param name="self">The collection to check.</param>
        /// <typeparam name="T">The type of item in the collection.</typeparam>
        /// <returns>True if the collection is not empty.</returns>
        public static bool IsNotEmpty<T>(this ICollection<T> self)
        {
            return self.Count > 0;
        }
    }
}