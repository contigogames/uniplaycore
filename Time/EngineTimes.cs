﻿namespace UniPlay.Core.Time
{
    public static class EngineTimes
    {
        public static readonly ITime ScaledTime = new ScaledTime();
        public static readonly ITime UnscaledTime = new UnscaledTime();
        public static readonly ITime FixedScaledTime = new FixedScaledTime();
        public static readonly ITime FixedUnscaledTime = new FixedUnscaledTime();
    }
}