﻿using UTime = UnityEngine.Time;

namespace UniPlay.Core.Time
{
    /// <summary>
    /// The game time unscaled.
    /// </summary>
    public class UnscaledTime : ITime
    {
        public float DeltaTime
        {
            get { return UTime.unscaledDeltaTime; }
        }

        public float Time
        {
            get { return UTime.unscaledTime; }
        }

        internal UnscaledTime() { }
    }
}