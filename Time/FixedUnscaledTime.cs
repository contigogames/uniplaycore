﻿using UTime = UnityEngine.Time;

namespace UniPlay.Core.Time
{
    /// <summary>
    /// The physics system time unscaled.
    /// </summary>
    public class FixedUnscaledTime : ITime
    {
        public float DeltaTime
        {
            get { return UTime.fixedUnscaledDeltaTime; }
        }

        public float Time
        {
            get { return UTime.fixedUnscaledTime; }
        }

        internal FixedUnscaledTime() { }
    }
}