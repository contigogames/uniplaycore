﻿using UTime = UnityEngine.Time;

namespace UniPlay.Core.Time
{
    /// <summary>
    /// The physics system time.
    /// </summary>
    public class FixedScaledTime : ITime
    {
        public float DeltaTime
        {
            get { return UTime.fixedDeltaTime; }
        }

        public float Time
        {
            get { return UTime.fixedTime; }
        }

        internal FixedScaledTime() { }
    }
}