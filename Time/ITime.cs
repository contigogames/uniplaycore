﻿namespace UniPlay.Core.Time
{
    /// <summary>
    /// Represents time information.
    /// </summary>
    public interface ITime
    {
        /// <summary>
        /// The time change.
        /// </summary>
        float DeltaTime { get; }
        
        /// <summary>
        /// The total time.
        /// </summary>
        float Time { get; }
    }
}