﻿using UTime = UnityEngine.Time;

namespace UniPlay.Core.Time
{
    /// <summary>
    /// The game time.
    /// </summary>
    public class ScaledTime : ITime
    {
        public float DeltaTime
        {
            get { return UTime.deltaTime; }
        }

        public float Time
        {
            get { return UTime.time; }
        }

        internal ScaledTime() { }
    }
}