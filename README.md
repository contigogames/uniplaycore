# README #

### What is this repository for? ###

* This repository holds the source code for UniPlayCore. Checkout the [wiki](../../wiki/Home) for more details.
* Version 2.0

### Contribution guidelines ###

* Contributions should include basic tests for any non-trivial piece of code. Use Unity in-editor tests for this.
* Code should match a similar style to that found in the rest of the repo.
* Contributions should be reviewed by an admin, as well as any peers whose code is affected by the given change.
* Be polite; Harassment, abuse, and generally rudeness will not be tolerated by **ANY** member.
* Be open minded and feel free to speak your thoughts! 

### Who do I talk to? ###

* (Owner) bpeake@contigo-games.com
* (Company) hello@contigo-games.com