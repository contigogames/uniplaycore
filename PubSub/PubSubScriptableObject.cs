﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Plugins.NewUniPlayCore.PubSub
{
    /// <summary>
    /// A pubsub that is a scriptable object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class PubSubScriptableObject<T>
        : ScriptableObject,
            IPubSub<T>
    {
        private readonly IPubSub<T> pubSubImplementation = new PubSub<T>();

        public IReadOnlyCollection<T> Publications
        {
            get { return pubSubImplementation.Publications; }
        }

        public event Action<T> OnPublicationAdded
        {
            add { pubSubImplementation.OnPublicationAdded += value; }
            remove { pubSubImplementation.OnPublicationAdded -= value; }
        }

        public event Action<T> OnPublicationRemoved
        {
            add { pubSubImplementation.OnPublicationRemoved += value; }
            remove { pubSubImplementation.OnPublicationRemoved -= value; }
        }

        public void PublicationAdded(T publication)
        {
            pubSubImplementation.PublicationAdded(publication);
        }

        public void PublicationRemoved(T publication)
        {
            pubSubImplementation.PublicationRemoved(publication);
        }
    }
}