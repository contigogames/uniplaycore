﻿using UniPlay.Core.View;

namespace Plugins.NewUniPlayCore.PubSub
{
    public class GameObjectPubSubProxy<T> : PubSubProxy<T>
    {
        public GameObjectPubSubProxy(IGameObjectView gameObjectView, IPubSub<T> parent) : base(parent)
        {
            gameObjectView.OnDestroyed += view => { UnAttach(); };
        }
    }
}