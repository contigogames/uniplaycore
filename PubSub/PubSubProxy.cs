﻿using System;
using System.Collections.Generic;

namespace Plugins.NewUniPlayCore.PubSub
{
    public class PubSubProxy<T> : IPubSub<T>
    {
        private IPubSub<T> pubSubImplementation;
        
        private readonly ISet<Action<T>> addEvents = new HashSet<Action<T>>();
        private readonly ISet<Action<T>> removeEvents = new HashSet<Action<T>>();
        
        private readonly ISet<T> publications = new HashSet<T>();
        
        public IReadOnlyCollection<T> Publications
        {
            get { return pubSubImplementation.Publications; }
        }

        public event Action<T> OnPublicationAdded
        {
            add
            {
                addEvents.Add(value);
                pubSubImplementation.OnPublicationAdded += value;
            }
            remove
            {
                addEvents.Remove(value);
                pubSubImplementation.OnPublicationAdded -= value;
            }
        }

        public event Action<T> OnPublicationRemoved
        {
            add
            {
                removeEvents.Add(value);
                pubSubImplementation.OnPublicationRemoved += value;
            }
            remove
            {
                removeEvents.Remove(value);
                pubSubImplementation.OnPublicationRemoved -= value;
            }
        }

        public PubSubProxy(IPubSub<T> parent)
        {
            pubSubImplementation = parent;
        }
        
        public void PublicationAdded(T publication)
        {
            publications.Add(publication);
            pubSubImplementation.PublicationAdded(publication);
        }

        public void PublicationRemoved(T publication)
        {
            publications.Remove(publication);
            pubSubImplementation.PublicationRemoved(publication);
        }

        public void UnAttach()
        {
            foreach (var publication in publications)
            {
                pubSubImplementation.PublicationRemoved(publication);
            }

            foreach (var addEvent in addEvents)
            {
                pubSubImplementation.OnPublicationAdded -= addEvent;
            }

            foreach (var removeEvent in removeEvents)
            {
                pubSubImplementation.OnPublicationRemoved -= removeEvent;
            }

            pubSubImplementation = null;
        }
    }
}