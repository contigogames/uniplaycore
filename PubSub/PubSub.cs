﻿using System;
using System.Collections.Generic;
using UniPlay.Core.Actions;

namespace Plugins.NewUniPlayCore.PubSub
{
    /// <summary>
    /// A default implementation of the pubsub system.
    /// </summary>
    public class PubSub<T> : IPubSub<T>
    {
        private readonly HashSet<T> publications = new HashSet<T>();

        private Action<T> _onPublicationAdded;
        private Action<T> _onPublicationRemoved;

        public IReadOnlyCollection<T> Publications
        {
            get { return publications; }
        }

        public event Action<T> OnPublicationAdded
        {
            add
            {
                _onPublicationAdded += value;
                foreach (var publication in publications)
                {
                    value.SafeInvoke(publication);
                }
            }
            remove { _onPublicationAdded -= value; }
        }

        public event Action<T> OnPublicationRemoved
        {
            add => _onPublicationRemoved += value;
            remove => _onPublicationRemoved -= value;
        }

        public void PublicationAdded(T publication)
        {
            if (!publications.Contains(publication))
            {
                publications.Add(publication);
                _onPublicationAdded?.SafeInvoke(publication);
            }
        }

        public void PublicationRemoved(T publication)
        {
            if (publications.Remove(publication))
            {
                _onPublicationRemoved?.SafeInvoke(publication);
            }
        }
    }
}