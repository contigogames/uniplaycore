﻿using System;
using System.Collections.Generic;

namespace Plugins.NewUniPlayCore.PubSub
{
    /// <summary>
    /// A generic pubsub that allows users to subscribe to a type of object being published.
    /// </summary>
    /// <typeparam name="T">The type of publication this pubsub supports.</typeparam>
    public interface IPubSub<T>
    {
        /// <summary>
        /// All current publications.
        /// </summary>
        IReadOnlyCollection<T> Publications { get; }
        
        /// <summary>
        /// Invoked when a publication is added. Any subscription to this event will be triggered for all existing
        /// publications on added.
        /// </summary>
        event Action<T> OnPublicationAdded;

        /// <summary>
        /// Invoked when a publication is removed.
        /// </summary>
        event Action<T> OnPublicationRemoved;

        /// <summary>
        /// Adds a publication.
        /// </summary>
        void PublicationAdded(T publication);

        /// <summary>
        /// Removes a publication.
        /// </summary>
        void PublicationRemoved(T publication);
    }
}