﻿using System;
using System.Collections.Generic;
using UniPlay.Core.Collection;
using UniPlay.Core.Constants;
using UnityEngine;
using UnityEngine.Assertions;

namespace UniPlay.Core.Verification
{
    /// <summary>
    /// A collection of helpful checks to do on values during creation.
    /// </summary>
    public static class PreCondition
    {
        /// <summary>
        /// Checks that a value is not null.
        /// </summary>
        /// <param name="value">The value to check.</param>
        public static void CheckIsNotNull(this object value)
        {
            Assert.IsNotNull(value);
        }

        /// <summary>
        /// Checks that a string is not null or empty.
        /// </summary>
        /// <param name="value">The string to check.</param>
        public static void CheckIsNotNullOrEmpty(this string value)
        {
            Assert.IsFalse(string.IsNullOrEmpty(value), "String is empty or null");
        }

        /// <summary>
        /// Checks that a collectin is not empty.
        /// </summary>
        /// <param name="collection">The collection to check.</param>
        public static void CheckIsNotEmpty<T>(this ICollection<T> collection)
        {
            collection.CheckIsNotNull();
            Assert.IsFalse(collection.IsEmpty(), "Collection is not empty.");
        }

        /// <summary>
        /// Check that an integer is not zero.
        /// </summary>
        /// <param name="value">The value to check.</param>
        public static void CheckNotZero(this int value)
        {
            Assert.AreNotEqual(value, 0);
        }

        /// <summary>
        /// CHeck that an integer is greater than zero.
        /// </summary>
        /// <param name="value">The value to check.</param>
        public static void CheckGreaterThanZero(this int value)
        {
            Assert.IsTrue(value > 0, string.Format("{0} is not > 0", value));
        }

        /// <summary>
        /// Check that an integer is greater than or equal to zero.
        /// </summary>
        /// <param name="value">The value to check.</param>
        public static void CheckNotNegative(this int value)
        {
            Assert.IsTrue(value >= 0, string.Format("{0} is not >= 0", value));
        }
        
        /// <summary>
        /// Check that an floating point is not zero.
        /// </summary>
        /// <param name="value">The value to check.</param>
        public static void CheckNotZero(this float value)
        {
            Assert.AreNotEqual(value, 0f);
        }

        /// <summary>
        /// CHeck that an floating point is greater than zero.
        /// </summary>
        /// <param name="value">The value to check.</param>
        public static void CheckGreaterThanZero(this float value)
        {
            Assert.IsTrue(value > 0, string.Format("{0} is not > 0", value));
        }
        
        /// <summary>
        /// Check that an floating point is greater than or equal to zero.
        /// </summary>
        /// <param name="value">The value to check.</param>
        public static void CheckNotNegative(this float value)
        {
            Assert.IsTrue(value >= 0, string.Format("{0} is not >= 0", value));
        }

        /// <summary>
        /// Checks that two values are not equal.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="other">The value to check against.</param>
        public static void CheckNotEqual<T>(this T value, T other)
        {
            Assert.AreNotEqual(value, other);
        }

        /// <summary>
        /// Check that two values are not approximately equal.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="other">The value to check against.</param>
        /// <param name="tolerance">The tolerance of the check.</param>
        public static void CheckNotApproximatelyEqual(
            this float value,
            float other,
            float tolerance = FloatConstants.DefaultTolerance)
        {
            Assert.AreNotApproximatelyEqual(value, other, tolerance);
        }

        /// <summary>
        /// Check that two values are not approximately equal.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="other">The value to check against.</param>
        /// <param name="tolerance">The tolerance of the check.</param>
        public static void CheckNotApproximatelyEqual(
            this Vector2 value,
            Vector2 other,
            float tolerance = Vector2Constants.DefaultMagnitudeTolerance)
        {
            Assert.IsTrue(
                (value - other).magnitude <= tolerance,
                string.Format("{0} and {1} are not approximently equal with tolerance {2}.", value, other, tolerance));
        }

        /// <summary>
        /// Check that two values are not approximately equal.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="other">The value to check against.</param>
        /// <param name="tolerance">The tolerance of the check.</param>
        public static void CheckNotApproximatelyEqual(
            this Vector3 value,
            Vector3 other,
            float tolerance = Vector3Constants.DefaultMagnitudeTolerance)
        {
            Assert.IsTrue(
                (value - other).magnitude <= tolerance,
                string.Format("{0} and {1} are not approximently equal with tolerance {2}.", value, other, tolerance));
        }

        /// <summary>
        /// Check a value against a generic condition.
        /// </summary>
        /// <param name="self">The value to check.</param>
        /// <param name="condition">The condition to check.</param>
        public static void CheckCondition<T>(this T self, Func<T, bool> condition)
        {
            Assert.IsTrue(condition(self), "condition(self) is not true.");
        }
    }
}